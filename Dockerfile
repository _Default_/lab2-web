From node as builder 
Run mkdir /project
Workdir /project
Copy . .
Run npm install
Run npm run build
From nginx
Expose 80
Copy --from=builder /project/build /usr/share/nginx/html
